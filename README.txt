Instruções para implantação

1. Wildfly
	1.1. Instalar módulo mysql 
		Copiar os arquivos em (resources/wildfly-modules/) para a pasta WILDLY_HOME\modules\system\layers\base\com\mysql\main.
		
	1.2. Configurar datasource e driver
		Modificar o arquivo WILDFLY_HOME\standalone\configuration\standalone.xml
		1.2.1. Sob a tag <datasources>, trocar os textos <USUARIO_MYSQL> e <SENHA_MYSQL>, pelo usuario e senha do MySql e adicionar no arquivo
		
				<xa-datasource jndi-name="java:/MysqlXADS" pool-name="MysqlXADS">
                    <xa-datasource-property name="ServerName">
                        localhost
                    </xa-datasource-property>
                    <xa-datasource-property name="DatabaseName">
                        azul
                    </xa-datasource-property>
                    <xa-datasource-class>com.mysql.cj.jdbc.MysqlXADataSource</xa-datasource-class>
                    <driver>mysql</driver>
                    <security>
                        <user-name><USUARIO_MYSQL></user-name>
                        <password><SENHA_MYSQL></password>
                    </security>
                    <validation>
                        <valid-connection-checker class-name="org.jboss.jca.adapters.jdbc.extensions.mysql.MySQLValidConnectionChecker"/>
                        <validate-on-match>true</validate-on-match>
                        <background-validation>false</background-validation>
                        <exception-sorter class-name="org.jboss.jca.adapters.jdbc.extensions.mysql.MySQLExceptionSorter"/>
                    </validation>
                </xa-datasource>

		1.2.2. Sob a tag <drivers>, adicionar
				<driver name="mysql" module="com.mysql">
					<driver-class>com.mysql.cj.jdbc.Driver</driver-class>
					<xa-datasource-class>com.mysql.cj.jdbc.MysqlXADataSource</xa-datasource-class>
				</driver>
				
2. Mysql
	2.1. Executar o script em resources/database.sql
	Criará o database "azul", o mesmo configurado em datasources
	
3. Implantar o projeto gerado através do Maven (utilizado versão 3.6.3)
	3.1. Comando mvn (mvn clean install)
	3.2. Implantar o war target/azul-cadastro.war