package br.com.miyasaki.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.miyasaki.exception.ValidationException;
import br.com.miyasaki.usuario.business.UsuarioRules;
import br.com.miyasaki.usuario.entity.Usuario;
import br.com.miyasaki.usuario.entity.UsuarioEndereco;
import br.com.miyasaki.usuario.service.UsuarioServiceBean;

@RunWith(MockitoJUnitRunner.class)
public class CadastroServiceTest {

	private static final String FORMATTED_CPF = "111.111.111-11";
	private static final String NORMALIZED_CPF = "11111111111";

	private static final String CEP_ITAQUAQUECETUBA = "08599-530";
	private static final String CIDADE_ITAQUAQUECETUBA = "Itaquaquecetuba";
	private static final String CEP_NOTEXISTS_NORMALIZED = "21345999";
	private static final String CEP_INVALID_LENGTH = "21349";
	
	private static final String EMAIL_OK = "renatom@gmail.com";

	@Mock
	UsuarioServiceBean serviceMock;
	
	@InjectMocks
	UsuarioRules service;
	
	
	@Test
	public void successTest() throws ValidationException {
		Usuario usuario = new Usuario();
		usuario.setEmail(EMAIL_OK);
		usuario.setCpf(FORMATTED_CPF);
		
		UsuarioEndereco usuarioEndereco = new UsuarioEndereco();
		usuarioEndereco.setUsuario(usuario);
		usuarioEndereco.setCep(CEP_ITAQUAQUECETUBA);
		usuarioEndereco.setLogradouro("rua x");
		
		Mockito.when(serviceMock.save(usuarioEndereco)).thenReturn(usuarioEndereco);
		
		br.com.miyasaki.usuario.api.model.Usuario saved = null;
		try {
			saved = service.save(usuarioEndereco);
		} catch (ValidationException e) {
			e.printStackTrace();
			fail("Erro ao salvar");
		}
		assertNotNull(saved);
		assertEquals(NORMALIZED_CPF, saved.getCpf());
		assertEquals(CIDADE_ITAQUAQUECETUBA, saved.getEndereco().getCidade());
	}

	@Test(expected = ValidationException.class)
	public void errorMissCepTest() throws ValidationException {
		Usuario usuario = new Usuario();
		usuario.setEmail(EMAIL_OK);
		usuario.setCpf(FORMATTED_CPF);
		
		UsuarioEndereco usuarioEndereco = new UsuarioEndereco();
		usuarioEndereco.setUsuario(usuario);
		usuarioEndereco.setLogradouro("rua x");
		
		service.save(usuarioEndereco);
		fail();
	}

	@Test(expected = ValidationException.class)
	public void errorMissEmailTest() throws ValidationException {
		Usuario usuario = new Usuario();
		usuario.setCpf(FORMATTED_CPF);
		
		UsuarioEndereco usuarioEndereco = new UsuarioEndereco();
		usuarioEndereco.setUsuario(usuario);
		usuarioEndereco.setLogradouro("rua x");
		
		service.save(usuarioEndereco);
		fail();
	}

	@Test(expected = ValidationException.class)
	public void errorCepNotExistsTest() throws ValidationException {
		Usuario usuario = new Usuario();
		usuario.setEmail(EMAIL_OK);
		usuario.setCpf(FORMATTED_CPF);
		
		UsuarioEndereco usuarioEndereco = new UsuarioEndereco();
		usuarioEndereco.setUsuario(usuario);
		usuarioEndereco.setLogradouro("rua x");
		usuarioEndereco.setCep(CEP_NOTEXISTS_NORMALIZED);
		
		try {
			service.save(usuarioEndereco);
		} catch (ValidationException e) {
			assertEquals("CEP não existe na base da viacep.com.br: " + CEP_NOTEXISTS_NORMALIZED, e.getMessage());
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}
		fail();
	}

	@Test(expected = ValidationException.class)
	public void errorCepInvalidLengthTest() throws ValidationException {
		Usuario usuario = new Usuario();
		usuario.setEmail(EMAIL_OK);
		usuario.setCpf(FORMATTED_CPF);
		
		UsuarioEndereco usuarioEndereco = new UsuarioEndereco();
		usuarioEndereco.setUsuario(usuario);
		usuarioEndereco.setLogradouro("rua x");
		usuarioEndereco.setCep(CEP_INVALID_LENGTH);
		
		try {
			service.save(usuarioEndereco);
		} catch (ValidationException e) {
			assertEquals("CEP deve ter 8 dígitos", e.getMessage());
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}
		fail();
	}


	@Test(expected = ValidationException.class)
	public void errorMissLogradouroTest() throws ValidationException {
		Usuario usuario = new Usuario();
		usuario.setEmail(EMAIL_OK);
		usuario.setCpf(FORMATTED_CPF);
		
		UsuarioEndereco usuarioEndereco = new UsuarioEndereco();
		usuarioEndereco.setUsuario(usuario);
		usuarioEndereco.setCep(CEP_ITAQUAQUECETUBA);
		
		service.save(usuarioEndereco);
		fail();
	}

	@Test(expected = ValidationException.class)
	public void errorCpfExistsTest() throws ValidationException {
		Usuario usuario = new Usuario();
		usuario.setEmail(EMAIL_OK);
		usuario.setCpf(FORMATTED_CPF);
		
		UsuarioEndereco usuarioEndereco = new UsuarioEndereco();
		usuarioEndereco.setUsuario(usuario);
		usuarioEndereco.setLogradouro("rua x");
		usuarioEndereco.setCep(CEP_ITAQUAQUECETUBA);
		
		
		List<Usuario> values = new ArrayList<Usuario>();
		values.add(new Usuario());
		Mockito.when(serviceMock.findByEmailOrCpf(EMAIL_OK, NORMALIZED_CPF)).thenReturn(values );
		try {
			service.save(usuarioEndereco);
		} catch (Exception e) {
			throw e;
		}
		fail();
	}
	

}
