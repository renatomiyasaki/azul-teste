package br.com.miyasaki.exception;

public class ValidationException extends Exception {

	private static final long serialVersionUID = 220414467589171913L;

	public ValidationException(String message) {
		super(message);
	}
}
