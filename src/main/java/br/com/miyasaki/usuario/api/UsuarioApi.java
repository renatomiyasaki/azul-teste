package br.com.miyasaki.usuario.api;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.miyasaki.exception.ValidationException;
import br.com.miyasaki.usuario.api.model.ModelApiResponse;
import br.com.miyasaki.usuario.api.model.Usuario;
import br.com.miyasaki.usuario.business.UsuarioRules;
import br.com.miyasaki.usuario.entity.UsuarioEndereco;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@Path("/usuario")
public class UsuarioApi {

	private static final String TOKEN = "azul";
	private static final Logger logger = LoggerFactory.getLogger(UsuarioApi.class);

	@Inject
	UsuarioRules usuarioRules;
	
	@GET
	@Path("/findByEmail")
	@Produces({ "application/json" })
	@Operation(summary = "Consulta usuário por e-mail", description = "", 
				tags = { "usuarios" })
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Usuário encontrado", content = @Content(schema = @Schema(implementation = Usuario.class))),
			@ApiResponse(responseCode = "404", description = "Usuário não encontrado") })
	public Response findByEmail(@HeaderParam("token") String token, 
			@NotNull @QueryParam("email")
	@Parameter(description = "E-mail a ser consultado") String email) {
		
		if (invalidToken(token)) {
			return Response.status(401).build();
		}
		Usuario ret = usuarioRules.findByEmail(email);
		if (ret == null) {
			ModelApiResponse response = new ModelApiResponse();
			response.code(404);
			response.message("Usuário não encontrado");
			response.type("Consulta");
			return Response.status(404).entity(response).build();
		}
		return Response.ok().entity(ret).build();
	}

	private boolean invalidToken(String token) {
		return !TOKEN.equals(token);
	}

	@POST
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	@Operation(summary = "Inclui um novo usuário", description = "", tags = { "usuarios" })
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Usuário incluído", content = @Content(schema = @Schema(implementation = br.com.miyasaki.usuario.api.model.Usuario.class))),
			@ApiResponse(responseCode = "400", description = "Dados inválidos") })
	public Response usuario(Usuario usuario,
			@HeaderParam("token") String token) {
		if (invalidToken(token)) {
			return Response.status(401).build();
		}
		Usuario ret = null;
		try {
			logger.info("usuario=" + usuario.toString());
			ret  = usuarioRules.save(UsuarioEndereco.toEntity(usuario));
		} catch (ValidationException e) {
			ModelApiResponse response = new ModelApiResponse();
			response.code(401);
			response.message(e.getMessage());
			response.type("Validação");
			return Response.status(400).entity(response).build();
		}
		return Response.ok().entity(ret).build();
	}

}