package br.com.miyasaki.usuario.api.model;

import java.util.Objects;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


public class Endereco   {
  private @Valid Integer usuarioId = null;
  private @Valid String logradouro = null;
  private @Valid String numero = null;
  private @Valid String cep = null;
  private @Valid String complemento = null;
  private @Valid String bairro = null;
  private @Valid String cidade = null;
  private @Valid String uf = null;

  /**
   **/
  public Endereco usuarioId(Integer usuarioId) {
    this.usuarioId = usuarioId;
    return this;
  }

  
  @JsonProperty("usuarioId")
  @JsonIgnoreProperties(ignoreUnknown = true)
  public Integer getUsuarioId() {
    return usuarioId;
  }
  public void setUsuarioId(Integer usuarioId) {
    this.usuarioId = usuarioId;
  }

  /**
   **/
  public Endereco logradouro(String logradouro) {
    this.logradouro = logradouro;
    return this;
  }

  
  @JsonProperty("logradouro")
  @JsonIgnoreProperties(ignoreUnknown = true)
  public String getLogradouro() {
    return logradouro;
  }
  public void setLogradouro(String logradouro) {
    this.logradouro = logradouro;
  }

  /**
   **/
  public Endereco numero(String numero) {
    this.numero = numero;
    return this;
  }

  
  @JsonProperty("numero")
  @JsonIgnoreProperties(ignoreUnknown = true)
  public String getNumero() {
    return numero;
  }
  public void setNumero(String numero) {
    this.numero = numero;
  }

  /**
   **/
  public Endereco cep(String cep) {
    this.cep = cep;
    return this;
  }

  
  @JsonProperty("cep")
  @JsonIgnoreProperties(ignoreUnknown = true)
  public String getCep() {
    return cep;
  }
  public void setCep(String cep) {
    this.cep = cep;
  }

  /**
   **/
  public Endereco complemento(String complemento) {
    this.complemento = complemento;
    return this;
  }

  
  @JsonProperty("complemento")
  @JsonIgnoreProperties(ignoreUnknown = true)
  public String getComplemento() {
    return complemento;
  }
  public void setComplemento(String complemento) {
    this.complemento = complemento;
  }

  /**
   **/
  public Endereco bairro(String bairro) {
    this.bairro = bairro;
    return this;
  }

  
  @JsonProperty("bairro")
  @JsonIgnoreProperties(ignoreUnknown = true)
  public String getBairro() {
    return bairro;
  }
  public void setBairro(String bairro) {
    this.bairro = bairro;
  }

  /**
   **/
  public Endereco cidade(String cidade) {
    this.cidade = cidade;
    return this;
  }

  
  @JsonProperty("cidade")
  @JsonIgnoreProperties(ignoreUnknown = true)
  public String getCidade() {
    return cidade;
  }
  public void setCidade(String cidade) {
    this.cidade = cidade;
  }

  /**
   **/
  public Endereco uf(String uf) {
    this.uf = uf;
    return this;
  }

  
  @JsonProperty("uf")
  @JsonIgnoreProperties(ignoreUnknown = true)
  public String getUf() {
    return uf;
  }
  public void setUf(String uf) {
    this.uf = uf;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Endereco endereco = (Endereco) o;
    return Objects.equals(usuarioId, endereco.usuarioId) &&
        Objects.equals(logradouro, endereco.logradouro) &&
        Objects.equals(numero, endereco.numero) &&
        Objects.equals(cep, endereco.cep) &&
        Objects.equals(complemento, endereco.complemento) &&
        Objects.equals(bairro, endereco.bairro) &&
        Objects.equals(cidade, endereco.cidade) &&
        Objects.equals(uf, endereco.uf);
  }

  @Override
  public int hashCode() {
    return Objects.hash(usuarioId, logradouro, numero, cep, complemento, bairro, cidade, uf);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Endereco {\n");
    
    sb.append("    usuarioId: ").append(toIndentedString(usuarioId)).append("\n");
    sb.append("    logradouro: ").append(toIndentedString(logradouro)).append("\n");
    sb.append("    numero: ").append(toIndentedString(numero)).append("\n");
    sb.append("    cep: ").append(toIndentedString(cep)).append("\n");
    sb.append("    complemento: ").append(toIndentedString(complemento)).append("\n");
    sb.append("    bairro: ").append(toIndentedString(bairro)).append("\n");
    sb.append("    cidade: ").append(toIndentedString(cidade)).append("\n");
    sb.append("    uf: ").append(toIndentedString(uf)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
