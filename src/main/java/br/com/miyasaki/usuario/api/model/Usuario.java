package br.com.miyasaki.usuario.api.model;

import java.util.Objects;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


public class Usuario   {
  private @Valid Integer usuarioId = null;
  private @Valid Endereco endereco = null;
  private @Valid String email = null;
  private @Valid String cpf = null;

  /**
   **/
  public Usuario usuarioId(Integer usuarioId) {
    this.usuarioId = usuarioId;
    return this;
  }

  
  @JsonProperty("usuarioId")
  @JsonIgnoreProperties(ignoreUnknown = true)
  public Integer getUsuarioId() {
    return usuarioId;
  }
  public void setUsuarioId(Integer usuarioId) {
    this.usuarioId = usuarioId;
  }

  /**
   **/
  public Usuario endereco(Endereco endereco) {
    this.endereco = endereco;
    return this;
  }

  
  @JsonProperty("endereco")
  @JsonIgnoreProperties(ignoreUnknown = true)
  public Endereco getEndereco() {
    return endereco;
  }
  public void setEndereco(Endereco endereco) {
    this.endereco = endereco;
  }

  /**
   **/
  public Usuario email(String email) {
    this.email = email;
    return this;
  }

  
  @JsonProperty("email")
  @JsonIgnoreProperties(ignoreUnknown = true)
  public String getEmail() {
    return email;
  }
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   **/
  public Usuario cpf(String cpf) {
    this.cpf = cpf;
    return this;
  }

  
  @JsonProperty("cpf")
  @JsonIgnoreProperties(ignoreUnknown = true)  
  public String getCpf() {
    return cpf;
  }
  public void setCpf(String cpf) {
    this.cpf = cpf;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Usuario usuario = (Usuario) o;
    return Objects.equals(usuarioId, usuario.usuarioId) &&
        Objects.equals(endereco, usuario.endereco) &&
        Objects.equals(email, usuario.email) &&
        Objects.equals(cpf, usuario.cpf);
  }

  @Override
  public int hashCode() {
    return Objects.hash(usuarioId, endereco, email, cpf);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Usuario {\n");
    
    sb.append("    usuarioId: ").append(toIndentedString(usuarioId)).append("\n");
    sb.append("    endereco: ").append(toIndentedString(endereco)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    cpf: ").append(toIndentedString(cpf)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
