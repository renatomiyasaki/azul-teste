package br.com.miyasaki.usuario.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;

import br.com.miyasaki.exception.ValidationException;
import br.com.miyasaki.usuario.api.model.Usuario;
import br.com.miyasaki.usuario.entity.UsuarioEndereco;
import br.com.miyasaki.usuario.service.UsuarioServiceBean;
import br.com.miyasaki.utils.CepUtils;
import br.com.miyasaki.vo.CepVO;

@Stateless
public class UsuarioRules {

	@Inject
	private UsuarioServiceBean usuarioBean;

	public Usuario findByEmail(@NotNull String email) {
		UsuarioEndereco ret = usuarioBean.findByEmail(email);
		if (ret == null)
			return null;
		return ret.toApiModel();
	}

	public Usuario save(UsuarioEndereco usuarioEndereco) throws ValidationException {
		normalizeData(usuarioEndereco);
		validateData(usuarioEndereco);
		fillEmptyAddressData(usuarioEndereco);
		
		UsuarioEndereco persisted = usuarioBean.save(usuarioEndereco);
		if (persisted == null)
			return null;
		return persisted.toApiModel();
	}
	
	private void fillEmptyAddressData(@Valid UsuarioEndereco usuarioEndereco) throws ValidationException {
		String cepValue = usuarioEndereco.getCep();
		
		CepVO cep = CepUtils.getDataByCep(cepValue);
		usuarioEndereco.setLogradouro(cep.getLogradouro());
		if (StringUtils.isAllBlank(usuarioEndereco.getBairro())) {
			usuarioEndereco.setBairro(cep.getBairro());
		}
		if (StringUtils.isAllBlank(usuarioEndereco.getCidade())) {
			usuarioEndereco.setCidade(cep.getLocalidade());
		}
		if (StringUtils.isAllBlank(usuarioEndereco.getUf())) {
			usuarioEndereco.setUf(cep.getUf());
		}
	}


	private void validateData(UsuarioEndereco usuarioEndereco) throws ValidationException {
		br.com.miyasaki.usuario.entity.Usuario usuario = usuarioEndereco.getUsuario();
		
		if (StringUtils.isAllBlank(usuario.getEmail())) {
			throw new ValidationException("E-mail deve ser preenchido");
		}
		
		if (StringUtils.isAllBlank(usuario.getCpf())) {
			throw new ValidationException("CPF deve ser preenchido");
		}
		
		if (StringUtils.isNumeric(usuario.getCpf()) && usuario.getCpf().length() != 11) {
			throw new ValidationException("CPF deve ter 11 dígitos");
		}
		
		if (StringUtils.isAllBlank(usuarioEndereco.getCep())) {
			throw new ValidationException("CEP deve ser preenchido");
		}
		
		if (StringUtils.isNumeric(usuarioEndereco.getCep()) && usuarioEndereco.getCep().length() != 8) {
			throw new ValidationException("CEP deve ter 8 dígitos");
		}
		
		if (StringUtils.isAllBlank(usuarioEndereco.getLogradouro())) {
			throw new ValidationException("Logradouro deve ser preenchido");
		}
		
		List<br.com.miyasaki.usuario.entity.Usuario> usuarios = usuarioBean.findByEmailOrCpf(usuario.getEmail(), usuario.getCpf());
		if (usuarios != null && usuarios.size() > 0) {
			throw new ValidationException(
					String.format("Já existe usuário com o e-mail %s ou CPF %s", usuario.getEmail(), usuario.getCpf()));
		}
		
		if (usuarioEndereco.getUf() != null && usuarioEndereco.getUf().length() > 5) {
			throw new ValidationException("UF não pode ser maior que 5 caracteres.");
		}
		
	}

	private void normalizeData(UsuarioEndereco usuarioEndereco) {
		br.com.miyasaki.usuario.entity.Usuario usuario = usuarioEndereco.getUsuario();
		String cpf = usuarioEndereco.getUsuario().getCpf();
		if (cpf != null) {
			cpf = cpf.replaceAll("[^0-9]", "");
			usuario.setCpf(cpf);
		}
		String cep = usuarioEndereco.getCep();
		if (cep != null) {
			cep = cep.replaceAll("[^0-9]", "");
			usuarioEndereco.setCep(cep);
		}
	}
}
