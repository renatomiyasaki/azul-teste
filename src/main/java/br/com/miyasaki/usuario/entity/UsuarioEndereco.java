package br.com.miyasaki.usuario.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.miyasaki.usuario.api.model.Endereco;

@Entity
@Table(name = "usuario_endereco")
@NamedQuery(name = UsuarioEndereco.FIND_BY_EMAIL_ID, query = UsuarioEndereco.FIND_BY_EMAIL_QUERY)
public class UsuarioEndereco {

	public static final String FIND_BY_EMAIL_ID = "UsuarioEndereco.findByEmail";
	public static final String FIND_BY_EMAIL_QUERY = "Select e From UsuarioEndereco e Where e.usuario.email = :email";
	

	@Id
	@Column(name = "id_usuario")
	private Integer usuarioId;
	
	@OneToOne(fetch = FetchType.LAZY)
    @MapsId
    @JoinColumn(name = "id_usuario")
	private Usuario usuario;
	
	@Column(name = "des_logradouro")
	private String logradouro;
	
	@Column(name = "des_numero")
	private String numero;
	
	@Column(name = "des_cep")
	private String cep;
	
	@Column(name = "des_complemento")
	private String complemento;
	
	@Column(name = "des_bairro")
	private String bairro;
	
	@Column(name = "des_cidade")
	private String cidade;
	
	@Column(name = "des_uf")
	private String uf;

	public int getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public br.com.miyasaki.usuario.api.model.Usuario toApiModel() {
		br.com.miyasaki.usuario.api.model.Usuario ret = this.getUsuario().toApiModel();
		Endereco end = new Endereco();
		end.setUsuarioId(ret.getUsuarioId());
		end.setLogradouro(this.getLogradouro());
		end.setNumero(this.getNumero());
		end.setComplemento(this.getComplemento());
		end.setBairro(this.getBairro());
		end.setCidade(this.getCidade());
		end.setUf(this.getUf());
		end.setCep(this.getCep());
		ret.setEndereco(end );
		
		return ret;
	}

	public static UsuarioEndereco toEntity(br.com.miyasaki.usuario.api.model.Usuario pUsuario) {
		UsuarioEndereco end = new UsuarioEndereco();
		end.setUsuarioId(pUsuario.getUsuarioId());
		end.setLogradouro(pUsuario.getEndereco().getLogradouro());
		end.setNumero(pUsuario.getEndereco().getNumero());
		end.setComplemento(pUsuario.getEndereco().getComplemento());
		end.setBairro(pUsuario.getEndereco().getBairro());
		end.setCidade(pUsuario.getEndereco().getCidade());
		end.setUf(pUsuario.getEndereco().getUf());
		end.setCep(pUsuario.getEndereco().getCep());
		Usuario usuario = new Usuario();
		usuario.setUsuarioId(pUsuario.getUsuarioId());
		usuario.setEmail(pUsuario.getEmail());
		usuario.setCpf(pUsuario.getCpf());
		end.setUsuario(usuario);
		
		return end;
	}
}
