package br.com.miyasaki.usuario.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "usuario")
@NamedQueries(value = {
		@NamedQuery(query = Usuario.FIND_BY_EMAIL_QUERY, name = Usuario.FIND_BY_EMAIL_ID),
		@NamedQuery(query = Usuario.FIND_BY_EMAIL_OR_CPF_QUERY, name = Usuario.FIND_BY_EMAIL_OR_CPF_ID)
})
public class Usuario {
	
	public static final String FIND_BY_EMAIL_QUERY = "Select u From Usuario u Where UPPER(u.email) like UPPER(:email)";
	public static final String FIND_BY_EMAIL_ID = "Usuario.findByEmail";
	public static final String FIND_BY_EMAIL_OR_CPF_QUERY = "Select u From Usuario u Where UPPER(u.email) like UPPER(:email) or u.cpf = :cpf";
	public static final String FIND_BY_EMAIL_OR_CPF_ID = "Usuario.findByEmailOrCpf";
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_usuario")
	private Integer usuarioId;

	@Column(name = "des_email")
	private String email;
	
	@Column(name = "des_cpf")
	private String cpf;

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public br.com.miyasaki.usuario.api.model.Usuario toApiModel() {
		br.com.miyasaki.usuario.api.model.Usuario usuario = new br.com.miyasaki.usuario.api.model.Usuario();
		usuario.setUsuarioId(this.getUsuarioId());
		usuario.setCpf(this.getCpf());
		usuario.setEmail(this.getEmail());
		return usuario;
	}

}
