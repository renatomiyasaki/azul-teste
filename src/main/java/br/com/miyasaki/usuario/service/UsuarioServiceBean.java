package br.com.miyasaki.usuario.service;

import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.miyasaki.exception.ValidationException;
import br.com.miyasaki.usuario.entity.Usuario;
import br.com.miyasaki.usuario.entity.UsuarioEndereco;

@Stateless
public class UsuarioServiceBean {

	private static final Logger logger = LoggerFactory.getLogger(UsuarioServiceBean.class);
	
	@PersistenceContext(unitName = "mysql-pu")
	private EntityManager em;
	
	@Transactional(value = TxType.REQUIRED)
	public UsuarioEndereco save(@Valid UsuarioEndereco usuarioEndereco) throws ValidationException {
		
		Usuario usuario = usuarioEndereco.getUsuario();
		em.persist(usuario);
		logger.info("Usuario Id: " + usuario.getUsuarioId());

		usuarioEndereco.setUsuario(usuario);
		em.persist(usuarioEndereco);
		
		return usuarioEndereco;
	}

	public List<Usuario> findByEmailOrCpf(String email, String cpf) {
		TypedQuery<Usuario> query = em.createNamedQuery(Usuario.FIND_BY_EMAIL_OR_CPF_ID, Usuario.class);
		
		try {
			List<Usuario> ret = query.setParameter("email", email)
					 				 .setParameter("cpf", cpf)
					 				 .getResultList();
			
			return ret;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return Collections.emptyList();
	}

	public UsuarioEndereco findByEmail(String email) {
		TypedQuery<UsuarioEndereco> query = em.createNamedQuery(UsuarioEndereco.FIND_BY_EMAIL_ID, UsuarioEndereco.class);
		query.setParameter("email", email);
		
		try {
			UsuarioEndereco ret = query.getSingleResult();
			
			return ret;
		} catch (NoResultException nre) {
			return null;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}
	
}
