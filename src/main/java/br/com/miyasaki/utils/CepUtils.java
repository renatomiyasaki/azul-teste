package br.com.miyasaki.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.miyasaki.exception.ValidationException;
import br.com.miyasaki.vo.CepVO;

public abstract class CepUtils {

	private static final Logger logger = LoggerFactory.getLogger(CepUtils.class);
	
	public static CepVO getDataByCep(String cep) throws ValidationException {
		String dataCepAPI = readCepApi(cep); 
		logger.info("CEP Result: " + dataCepAPI);
		
		JSONObject obj = new JSONObject(dataCepAPI);
		
		if (obj.has("erro")) {
			throw new ValidationException("CEP não existe na base da viacep.com.br: " + cep);
		}
		CepVO ret = new CepVO();
		ret.setCep(obj.getString("cep"));
		ret.setLogradouro(obj.getString("logradouro"));
		ret.setComplemento(obj.getString("complemento"));
		ret.setBairro(obj.getString("bairro"));
		ret.setLocalidade(obj.getString("localidade"));
		ret.setUf(obj.getString("uf"));
		ret.setIbge(obj.getString("ibge"));
		ret.setGia(obj.getString("gia"));
		ret.setDdd(obj.getString("ddd"));
		ret.setSiafi(obj.getString("siafi"));
		return ret ;
	}

	private static String readCepApi(String cep) throws ValidationException {
		StringBuilder result = new StringBuilder();

		String urlAPI = "http://viacep.com.br/ws/" + cep + "/json/";
        try {
            URL url = new URL(urlAPI);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");

            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw new ValidationException("Erro ao consultar CEP: " + ex.getMessage());
        }
		return result.toString();
	}
}
