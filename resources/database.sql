create database azul;

use azul;

create table usuario (
  id_usuario int not null primary key auto_increment,
  des_email varchar(100) not null,
  des_cpf varchar(11) not null
);

create table usuario_endereco (
  id_usuario int not null primary key,
  des_logradouro varchar(50) not null,
  des_numero varchar(20) not null,
  des_complemento varchar(50),
  des_bairro varchar(30),
  des_cidade varchar(30),
  des_uf varchar(5),
  des_cep varchar(15) not null);
  
alter table usuario_endereco add constraint fk_usuario__usuario_endereco foreign key (id_usuario) references usuario(id_usuario);
